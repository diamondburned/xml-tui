package main

import (
	"github.com/beevik/etree"
	"./src/piperead"
	"github.com/rivo/tview"
	"github.com/gdamore/tcell"
	"log"
	"os"
)

func errCheck(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func load() *etree.Document {
	stdin := piperead.Read()
	file := etree.NewDocument()
	
	if err := file.ReadFromString(stdin); err != nil {
		panic(err)
	}

	file.IndentTabs()

	return file
}

var (
	app  = tview.NewApplication()
	file = load()
)

func main() {
	list := listkeys(app, nil, file.ChildElements())

	app.SetRoot(list, true)
	if err := app.Run(); err != nil {
		panic(err)
	}

	//var parent string
	//tui := ymtui.NewView(func(view *ymtui.View, clicked string) {
	//	if chose := file.SelectElement(clicked); chose != nil {
	//		parent = chose.GetPath()
	//		view.SetItems(listkeys(chose.ChildElements()))
	//	} else {
	//		if chose := file.FindElement("/" + parent + "/" + clicked + "/*"); chose != nil {
	//			view.SetItems(listkeys(chose.ChildElements()))
	//		} else {
	//			println("/" + parent + "/" + clicked)
	//			return
	//		}
	//	}
	//}, listkeys(file.ChildElements()))

	//ymtui.Start(tui)


} 

func listkeys(app *tview.Application, parent *etree.Element, elements []*etree.Element) *tview.List {
	list := tview.NewList()
	list.ShowSecondaryText(true)
	list.SetSelectedFunc(func(index int, tag, desc string, key rune) {
		redraw(tag)
	})

	if parent != nil {
		list.AddItem(parent.GetPath(), "Go back...", 0, nil)
	}

	if len(elements) < 1 {
		var kvs = parent.FindElements("*")
		list.SetSelectedFunc(func(index int, tag, desc string, key rune) {
			prompt(desc, tag)
		})

		for _, elm := range kvs {
			list.AddItem(elm.GetPath(), "    " + elm.Text(), 0, nil)
		}
	} else {
		for _, key := range elements {
			var input = "    " + key.Tag

			if key.Space != "" {
				input += " " + key.Space
			}

			for _, attribute := range key.Attr {
				input += " " + attribute.Key + "=" + attribute.Value
			}

			list.AddItem(key.GetPath(), input, 0, nil)
		}
	}

	list.AddItem("Quit", "    Quit without saving", 'q', func() { 
		app.Stop()
	})

	list.AddItem("Save", "    Quit and save", 's', func() { 
		app.Stop()
		file.WriteTo(os.Stdout)
	})

	return list
}

func redraw(path string) {
	var list *tview.List

	if path == "" {
		list = listkeys(app, nil, file.ChildElements())
	} else {
		if chose := file.FindElement(path); chose != nil {
			list  = listkeys(app, chose.Parent(), chose.ChildElements())
		} else {
			println("/" + path)
			os.Exit(1)
		}
	}
	
	app.SetRoot(list, true)
	app = app.Draw()
}

func prompt(placeholder, oldloc string) {
	var field = tview.NewInputField()
		field.SetPlaceholder(placeholder)
		field.SetDoneFunc(func (key tcell.Key) {
			if key == tcell.KeyEnter {
				file.FindElement(oldloc).SetText(field.GetText())
			}

			redraw(oldloc)
		})

	var frame = tview.NewFrame(field)

	frame = frame.AddText("Changing: " + oldloc, true, 0, tcell.ColorDefault)
	frame = frame.AddText("Default value: " + placeholder, false, 0, tcell.ColorDefault)

	app.SetRoot(frame, true)
	app = app.Draw()

}