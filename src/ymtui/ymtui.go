package ymtui

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/gdamore/tcell"
)

// func main() {
// 	// Example:
// 	whatever := []string{"Ok", "no", "u"}
// 	temp := []string{"Not", "Ok", "no", "u"}
// 	other := []string{1, "Ok", "no", "u"}
// 	view := NewView(func(view *View, something string) {
// 		view.SetItems(other)
// 		temp = other
// 		other = whatever
// 		whatever = temp
// 	}, whatever)
// 	Start(view)
// }

// Start starts the tui
func Start(view *View) {
	screen, err := tcell.NewScreen()
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	if err = screen.Init(); err != nil {
		println(err.Error())
		os.Exit(1)
	}

	events := make(chan tcell.Event)
	go func() {
		for {
			events <- screen.PollEvent()
		}
	}()

	// width, length := screen.Size()

loop:
	for {
		select {
		case <-time.After(time.Millisecond * 16):
			screen.Clear()
			view.update(screen)
			screen.Show()
		case ev := <-events:
			switch ev := ev.(type) {
			case *tcell.EventKey:
				switch ev.Key() {
				case tcell.KeyRune:
					switch ev.Rune() {
					case 'q':
						break loop
					}
				case tcell.KeyCtrlL:
					screen.Clear()
					screen.Sync()
				default:
					view.handle(screen, ev)
				}
			case *tcell.EventResize:
				// width, length = screen.Size()
				screen.Sync()
			}
		}
	}
	screen.Fini()
}

type View struct {
	items          []string
	dostuff        func(*View, string)
	head, selected int
}

func NewView(doSomething func(*View, string), items []string) *View {
	return &View{items: items, dostuff: doSomething}
}

func (view *View) SetItems(items []string) {
	view.items = items
}

func (view *View) update(buf tcell.Screen) {
	style := tcell.StyleDefault.Dim(true).Underline(true)
	for i := view.head; i < len(view.items); i++ {
		if i == view.selected {
			view.draw(buf, i, 0, i-view.head, view.items[i], style)
		} else {
			view.draw(buf, i, 0, i-view.head, view.items[i], tcell.StyleDefault)
		}
	}
}

func (view *View) draw(buf tcell.Screen, j, x, y int, element string, style tcell.Style) {
	number := []rune(strconv.Itoa(j))
	for i := 0; i < len(number); i++ {
		buf.SetContent(x+i, y, number[i], nil, style)
	}
	buf.SetContent(x+len(number), y, '.', nil, style)
	buf.SetContent(x+len(number)+1, y, ' ', nil, style)

	name := []rune(fmt.Sprintf("%v", element))
	for i := 0; i < len(name); i++ {
		buf.SetContent(x+i+len(number)+2, y, name[i], nil, style)
	}
}

func (view *View) handle(buf tcell.Screen, ev tcell.Event) {
	_, length := buf.Size()
	switch ev := ev.(type) {
	case *tcell.EventKey:
		switch ev.Key() {
		case tcell.KeyEnter:
			view.dostuff(view, view.items[view.selected])
			view.update(buf)
			buf.Show()
		case tcell.KeyDown:
			down(view, length)
		case tcell.KeyUp:
			up(view, length)
		case tcell.KeyRune:
			switch ev.Rune() {
			case 'u':
				view.head -= length
				if view.head < 0 {
					view.head = 0
				}
				view.selected = view.head
			case 'd':
				view.head += length
				if view.head > len(view.items) {
					view.head = len(view.items) - (len(view.items) % length)
				}
				view.selected = view.head
			case 'g':
				view.selected = 0
				view.head = 0
			case 'G':
				view.selected = len(view.items) - 1
				view.head = (view.selected / length) * length
			case 'j':
				down(view, length)
			case 'k':
				up(view, length)
			}
		}
	}
}

func up(view *View, length int) {
	view.selected = ((view.selected - 1) + len(view.items)) % len(view.items)
	view.head = (view.selected / length) * length
}

func down(view *View, length int) {
	view.selected = (view.selected + 1) % len(view.items)
	view.head = (view.selected / length) * length
}