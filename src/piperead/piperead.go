package piperead

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

// Read from pipe 
// https://flaviocopes.com/go-shell-pipes/
func Read() string {
	info, err := os.Stdin.Stat()
    if err != nil {
        panic(err)
    }

	if info.Mode()&os.ModeNamedPipe == 0 {
		fmt.Println("The command is intended to work with pipes.")
		fmt.Println("Usage: cat file.xml | ./xml-tui")
		os.Exit(1)
		return ""
	}

	reader := bufio.NewReader(os.Stdin)
	var output []rune

	for {
		input, _, err := reader.ReadRune()
		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)
	}

	return string(output)
}
